from rest_framework.routers import DefaultRouter
from api.views import ShortLinkViewSet

app_name = "api"
router = DefaultRouter()

router.register(r"short-link", viewset=ShortLinkViewSet, basename="short-link")

urlpatterns = []
urlpatterns += router.urls
